import 'package:flutter/material.dart';
import 'package:sizeier/desktp.dart';
import 'package:sizeier/mobile.dart';
import 'package:sizeier/responsive.dart';

class HomePage extends StatefulWidget {
  const HomePage({Key? key}) : super(key: key);

  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  @override
  Widget build(BuildContext context) {
    final currentWidth = MediaQuery.of(context).size.width;
    final currentHeight = MediaQuery.of(context).size.height;
    return Scaffold(
      backgroundColor: myChangeColor(currentWidth,currentHeight), //-> Change in color according to width and height
      //(currentWidth<600 || currentHeight<600)? Colors.deepPurpleAccent : Colors.deepOrangeAccent
      body: responsive( //-> change in color according to size mobile /desktop
      mobilelayout: mobile(),
        destopbody: desktp(),
        //myChangeColor(currentWidth, currentHeight);
      ),


      // Center(
      //     child:Column(
      //         children:[
      //           SizedBox(height: currentHeight/2,),
      //           Text(currentWidth.toString(), textAlign: TextAlign.center,),
      //           Text(currentHeight.toString(),textAlign: TextAlign.center,),
      //         ]
      //     )
      // ),
    );
  }

   myChangeColor(double currentWidth,double currentHeight) {
    if(currentWidth<600 && currentHeight<600){
      Color c = const Color(0xFF42A5F5);
      return c;
    }else if(currentWidth<400 || currentHeight<400){
      Color c =  Colors.deepPurpleAccent;
      return c;
    }else if(currentWidth>500 || currentHeight>500){
      Color color1 = Colors.greenAccent;
      return color1;
    }
  }
}
