import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';

class mobile extends StatelessWidget {
  const mobile({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.greenAccent,
      appBar: AppBar(
        title: Text("M o b i l e View"),
        centerTitle: true,
      ),
      body: Column(
        children: [
          bodyContentIndex(),
          bitCoinListView(),
        ],
      ),
    );
  }

  bodyContentIndex() {
    return Padding(
      padding: const EdgeInsets.all(8.0),
      child: AspectRatio(
        aspectRatio: 16 / 9,
        child: Container(height: 250, color: Colors.deepPurple),
      ),
    );
  }

  bitCoinListView() {
    return Expanded(
        child: ListView.builder(
            itemCount: 8,
            itemBuilder: (context, index) {
              return Padding(
                padding: const EdgeInsets.all(8.0),
                child: Container(
                  color: Colors.deepPurple[300],
                  height: 120,
                ),
              );
            }));
  }
}
