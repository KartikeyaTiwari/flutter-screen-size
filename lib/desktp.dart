import 'package:flutter/material.dart';

class desktp extends StatelessWidget {
  const desktp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.greenAccent,
      appBar: AppBar(
        title: Text("DESKTOP View"),
        centerTitle: true,
      ),
      body: Row(
        children: [
          Expanded(
            child: Column(
              children: [
                bodyContentIndex(),
                bitCoinListView(),
              ],
            ),
          ),
          sidebar()
        ],
      ),
    );
  }

  bodyContentIndex() {
    return Padding(
      padding: const EdgeInsets.all(8.0),
      child: AspectRatio(
        aspectRatio: 30 / 10,
        child: Container(height: 250, color: Colors.deepPurple),
      ),
    );
  }

  bitCoinListView() {
    return Expanded(
        child: ListView.builder(
            itemCount: 8,
            itemBuilder: (context, index) {
              return Padding(
                padding: const EdgeInsets.all(8.0),
                child: Container(
                  color: Colors.deepPurple[300],
                  height: 120,
                ),
              );
            }));
  }

  sidebar() {
    return Container(width: 200,color: Colors.deepPurple[300],);
  }
}
