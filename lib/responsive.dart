import 'package:flutter/material.dart';
import 'package:sizeier/dimension.dart';

class responsive extends StatelessWidget {
  final Widget mobilelayout;
  final Widget destopbody;

  responsive({required this.mobilelayout,required this.destopbody});

  @override
  Widget build(BuildContext context) {
    return LayoutBuilder(builder:(context,constraints){
      if(constraints.maxWidth <mobileWidth){
        return mobilelayout;
      }else{
        return destopbody;
      }
    },
    );

  }
}
